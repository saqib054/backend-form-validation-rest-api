const mongoose = require('mongoose');

const CustomerSchema = new mongoose.Schema({
  first_name: {
    type: String,
    required: true
  },
  last_name: {
    type: String
  },
  email: {
    type: String,
    required: true
  },
  phone: {
    type: Number,
    required: true
  },
  address: {
    type: String
  },
  gender: {
    type: String,
    enum: ['Male', 'Female'],
    required: true
  },
  customer_query: {
    type: String
  }
});

const Customer = mongoose.model('Customer', CustomerSchema);
module.exports = Customer;
