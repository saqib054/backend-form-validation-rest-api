const mongoose = require('mongoose');

const connectDatabase = async () => {
  try {
    const mongooseConnect = await mongoose.connect(process.env.mongoURI, {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
      useUnifiedTopology: true,
      useUnifiedTopology: true,
      keepAlive: 1,
      connectTimeoutMS: 30000
    });

    console.log(
      `MongoDB connected successfully: ${mongooseConnect.connection.host}`
    );
  } catch (error) {
    console.log('Could not connect to database', error);
    process.exit(1);
  }
};

module.exports = connectDatabase;
