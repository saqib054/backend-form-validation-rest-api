const { stringify } = require('querystring');
const fetch = require('node-fetch');
const { ErrorHandler } = require('../middleware/error');

module.exports = async (req, res, next) => {
  try {
    if (!req.body.captcha) {
      throw new ErrorHandler(400, 'Please select captcha');
    }

    // Verify URL
    const query = stringify({
      secret: process.env.captchaSecretKey,
      response: req.body.captcha,
      remoteip: req.connection.remoteAddress,
    });
    const verifyURL = `${process.env.googleReCaptchaUrl}?${query}`;

    // Make a request to verifyURL
    await fetch(verifyURL).then((res) => res.json());

    next();
  } catch (error) {
    res.status(400).json(error);
  }
};
