const Customer = require('../models/customer.model');
const { ErrorHandler } = require('../middleware/error');
const { body, validationResult } = require('express-validator');
const { stringify } = require('querystring');
const fetch = require('node-fetch');

exports.createCustomer = async (req, res, next) => {
  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      throw new ErrorHandler(422, { validationErrors: errors.array() });
    }

    const {
      firstName,
      lastName,
      email,
      phone,
      address,
      gender,
      customerQuery
    } = req.body;

    const customer = new Customer({
      first_name: firstName,
      last_name: lastName,
      email,
      phone,
      address,
      gender,
      customer_query: customerQuery
    });
    await customer.save();
    res.status(201);
    res.json({ message: 'New customer created..!' });
  } catch (error) {
    next(error);
  }
};

exports.validate = method => {
  switch (method) {
    case 'createCustomer': {
      return [
        body('firstName')
          .exists()
          .trim()
          .isAlpha()
          .withMessage('Name should only contain letters')
          .notEmpty(),
        body('lastName')
          .optional()
          .trim(),
        body('email')
          .exists()
          .trim()
          .isEmail()
          .withMessage('Enter valid email address'),
        body('phone')
          .exists()
          .isInt()
          .withMessage('Enter only numbers')
          .isLength({ min: 10, max: 10 })
          .withMessage('Enter phone in correct format'),
        body('address').optional(),
        body('gender', 'Gender is required').exists(),
        body('customerQuery').optional()
      ];
    }
  }
};
