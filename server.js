const express = require('express');
const dotenv = require('dotenv');
const connectDatabase = require('./database/dbConfig');
const bodyParser = require('body-parser');

const { handleError } = require('./middleware/error');
const cors = require('cors');

// Load env vars
dotenv.config({ path: './config/.env' });

// Declaring routes variables
const customer = require('./routes/customer.router');

const app = express();
app.use(cors());

// body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Customer routes
app.use('/api/', customer);

app.get('/', (req, res) => {
  res.status(200).json({
    message: 'Welcome to this App API'
  });
});

// Connect to MongoDB
connectDatabase();

const port = process.env.PORT || 5000;
app.use((err, req, res, next) => {
  handleError(err, res);
});

app.listen(port, () => console.log(`Server running on port ${port}`));

module.exports = app;
