const express = require('express');
const router = express.Router();

const {
  createCustomer,
  validate
} = require('../contollers/customer.controller');
const captchaValidation = require('../middleware/captchaValidation');

/**
 * @api {post} /customer Create customer
 * @apiName Create new customer
 *
 * @apiParam  {String} [firstname] First name
 * @apiParam  {String} [lastname] Last name
 * @apiParam  {String} [email] Email
 * @apiParam  {String} [phone] Phone number
 * @apiParam  {String} [gender] Gender
 *
 * @apiSuccess (200) {Object} `Customer` object
 */
router.post(
  '/customer',
  captchaValidation,
  validate('createCustomer'),
  createCustomer
);

module.exports = router;
